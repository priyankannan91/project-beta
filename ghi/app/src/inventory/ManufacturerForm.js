import React, { useState, useEffect } from 'react';

function ManufacturerForm() {
  const [name, setName] = useState("")

  const handleNameChange = (event) => {
    setName(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;

    const url = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
    },
  };

  const response = await fetch(url, fetchConfig);

  if (response.ok) {
    setName("");
  }
}

  useEffect(() => {
  }, [])

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default ManufacturerForm;
