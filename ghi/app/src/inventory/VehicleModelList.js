import { useState, useEffect } from 'react'

function VehicleModelList() {
    const [models, setModels] = useState([]);

    async function fetchModels() {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const parsedJson = await response.json();
            setModels(parsedJson.models);
        }
    }

    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <div>
            <h2>Models</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img src={ model.picture_url } alt={ model.picture_url } width='150' />
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default VehicleModelList;
