import React, { useEffect, useState } from 'react';

function AutomobileList(){
  const [automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const automobileUrl = "http://localhost:8100/api/automobiles/";

    const automobileResponse = await fetch(automobileUrl);

    if (automobileResponse.ok) {
      const data = await automobileResponse.json();
      const soldAutomobiles = data.autos.map((automobile) => {
        return {
          ...automobile,
          isSold: automobile.sold === true,
        };
      });
      setAutomobiles(soldAutomobiles);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map(auto => {
          return(
            <tr key={ auto.vin }>
              <td>{ auto.vin }</td>
              <td>{ auto.color }</td>
              <td>{ auto.year }</td>
              <td>{ auto.model.name }</td>
              <td>{ auto.model.manufacturer.name }</td>
              <td>{ auto.isSold ? 'Yes': 'No' }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
export default AutomobileList;
