import React, { useEffect, useState } from "react";

function VehicleModel(){
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturer, setManufacturer] = useState('');

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleChangepictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {};

    data.name = name;
    data.picture_url = pictureUrl;
    data.manufacturer_id = manufacturer;

    const vehicleModelUrl = "http://localhost:8100/api/models/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const vehicleModelResponse = await fetch(vehicleModelUrl, fetchOptions);

    if (vehicleModelResponse.ok) {
      setName('');
      setPictureUrl('');
      setManufacturer('');
    }
  }

  const fetchData = async () => {
    const manfacturerUrl = "http://localhost:8100/api/manufacturers/";
    const manfacturerResponse = await fetch(manfacturerUrl);
    if (manfacturerResponse.ok) {
      const data = await manfacturerResponse.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeName} value={name} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangepictureUrl} value={pictureUrl} placeholder="Image" required name="picture_url" type="text" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Image</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeManufacturer} value={manufacturer} required name="manufacturer" className="form-select" id="manufacturer">
                <option value="">Choose a Manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  )
                })}
              </select>
            </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}
export default VehicleModel;
