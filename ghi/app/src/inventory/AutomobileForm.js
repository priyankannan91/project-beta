import React, { useEffect, useState } from 'react';

function AutomobileForm(){
  const [models, setModels] = useState([]);
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');

  const handleChangeColor = (event) => {
     const value = event.target.value;
     setColor(value);
  }

  const handleChangeYear = (event) => {
    const value = event.target.value;
    setYear(value);
  }

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleChangeModel = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;

    const automobileUrl = "	http://localhost:8100/api/automobiles/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json"
      },
    };

    const automobileResponse = await fetch(automobileUrl, fetchOptions)

    if (automobileResponse.ok) {
      setColor('');
      setYear('');
      setVin('');
      setModel('');
    }
  };

  const fetchData = async () => {
    const vechileModelUrl = "http://localhost:8100/api/models/";
    const vechileModelResponse = await fetch(vechileModelUrl);

    if (vechileModelResponse.ok) {
      const data = await vechileModelResponse.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required name="color" type="text" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeYear} value={year} placeholder="Year" required name="year" type="text" id="year" className="form-control" />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeVin} value={vin} placeholder="Vin" required name="vin" type="text" id="vin" className="form-control" />
              <label htmlFor="vin">Vin</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeModel} value={model} required name="name" className="form-select" id="name">
                <option value="">Choose a Model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AutomobileForm;
