import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import ManufacturerList from './inventory/ManufacturerList';
import ManufacturerForm from './inventory/ManufacturerForm';
import VehicleModelList from './inventory/VehicleModelList';
import VehicleModelForm from './inventory/VehicleModelForm';
import AutomobileList from './inventory/AutomobileList';
import AutomobileForm from './inventory/AutomobileForm';
import SalespeopleList from './sales/SalespersonList';
import SalespersonForm from './sales/SalespersonForm';
import CustomerForm from './sales/CustomerForm';
import CustomerList from './sales/CustomerList';
import SalesForm from './sales/SalesForm';
import SalesList from './sales/SalesList';
import SalespersonHistory from './sales/SalespersonHistory';
import ServiceAppointmentList from './services/ServiceAppointmentList';
import ServiceHistory from './services/ServiceHistory';
import ServiceAppointmentForm from './services/ServiceAppointmentForm';
import TechnicianForm from './services/TechnicianForm';
import TechnicianList from './services/TechnicianList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="manufacturers/">
              <Route index element={<ManufacturerList />} />
              <Route path="create/" element={<ManufacturerForm />} />
            </Route>
            <Route path="models/">
              <Route index element={<VehicleModelList />} />
              <Route path="create/" element={<VehicleModelForm />} />
            </Route>
            <Route path="automobiles/">
              <Route index element={<AutomobileList />} />
              <Route path="create/" element={<AutomobileForm />} />
            </Route>
            <Route path="salespeople/">
              <Route index element={<SalespeopleList />} />
              <Route path="create/" element={<SalespersonForm />} />
            </Route>
            <Route path="customers/">
              <Route index element={<CustomerList />} />
              <Route path="create/" element={<CustomerForm />} />
            </Route>
            <Route path="sales/">
              <Route index element={<SalesList />} />
              <Route path="create/" element={<SalesForm />} />
              <Route path="history/" element={<SalespersonHistory />} />
            </Route>
            <Route path="appointments/">
              <Route index element={<ServiceAppointmentList />} />
              <Route path="create/" element={<ServiceAppointmentForm />} />
              <Route path="history/" element={<ServiceHistory />} />
            </Route>
            <Route path="technician/">
              <Route index element={<TechnicianList />} />
              <Route path="create/" element={<TechnicianForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
