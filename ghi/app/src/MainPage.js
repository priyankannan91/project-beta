import React from 'react';
import videoBG from './assets/CarCarVideo.mp4'
import './index.css'

function MainPage() {
  return (
    <>
      <div className="mainBG">
          <video src={videoBG} autoPlay loop muted />
      </div>
      <div className="text-container px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarLink Central</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
  </>
  );
}

export default MainPage;
