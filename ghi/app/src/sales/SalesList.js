import { useState, useEffect } from 'react'

function SalesList() {
    const [sales, setSales] = useState([]);
    
    async function fetchSales(){
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const parsedJson = await response.json();
            setSales(parsedJson.sales);
        }
    }
    
    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <div>
            <h2>Sales</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map((sale) => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name}</td>
                            <td>{sale.customer.first_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default SalesList;