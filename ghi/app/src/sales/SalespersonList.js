import { useState, useEffect } from 'react'

function SalespeopleList(){
  const [salespeople, setSalesPeople] = useState([]);

  async function fetchSalesPeople() {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
      const parsedJson = await response.json();
      setSalesPeople(parsedJson.salespeople);
    }
  }

  useEffect(() => {
    fetchSalesPeople();
  }, []);

  return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Employee ID</th>
      </tr>
    </thead>
    <tbody>
      {salespeople.map(person => {
        return (
          <tr key={person.id}>
            <td>{ person.first_name }</td>
            <td>{ person.last_name }</td>
            <td>{ person.employee_id }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
  );
}

export default SalespeopleList;
