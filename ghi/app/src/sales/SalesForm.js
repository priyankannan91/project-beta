import React, {useEffect, useState} from 'react';

function SalesForm() {
  const [automobile, setAutomobile] = useState('');
  const [salesperson, setSalesPerson] = useState('');
  const [customer, setCustomer] = useState('');
  const [price, setPrice] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salespersons, setSalesPersons] = useState([]);
  const [customers, setCustomers] = useState([]);

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  }

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleSalesPriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      automobile: automobile,
      salesperson: salesperson,
      customer: customer,
      price: price,
    };

    const saleUrl = "http://localhost:8090/api/sales/";
    const updateAutomobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`

    const saleConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        }
    };

    const automobileConfig = {
      method : "put",
      body: JSON.stringify({"sold": true}),
      headers : {
        "Content-Type" : 'application/json',
      }
    }

    const salesResponse = await fetch(saleUrl, saleConfig);

    const automobilesResponse = await fetch(updateAutomobileUrl, automobileConfig)

    if ((salesResponse.ok) && (automobilesResponse)) {
      setAutomobile('');
      setCustomer('');
      setSalesPerson('');
      setPrice('');
      fetchAutomobiles();
    } else {
      console.error("Failed to record the sale!");
    }
  } 
  
    
const fetchAutomobiles = async () => {
  const automobileUrl = 'http://localhost:8100/api/automobiles/';
  const response = await fetch(automobileUrl);

  if (response.ok) {
    const data = await response.json();
    setAutomobiles(data.autos);
  }
}

const fetchSalesPersons = async () => {
  const salespersonUrl = 'http://localhost:8090/api/salespeople/';
  const response = await fetch(salespersonUrl);

  if (response.ok) {
    const data = await response.json();
    setSalesPersons(data.salespeople);
  }
}

const fetchCustomers = async () => {
  const customerUrl = 'http://localhost:8090/api/customers/';
  const response = await fetch(customerUrl);

  if (response.ok) {
    const data = await response.json();
    setCustomers(data.customers);
  }
}

useEffect(() => {
    fetchSalesPersons();
    fetchAutomobiles();
    fetchCustomers();
}, []);

  return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a sales record</h1>
            <form onSubmit={handleSubmit} id="create-sales-record-form">
                <div className="mb-3">
                    <select value={automobile} onChange={handleAutomobileChange} required name="automobile" className="form-select">
                    <option value="">Choose a Automobile</option>
                    {automobiles.map(automobile => {
                      if (automobile.sold === false) {
                        return (
                          <option key={automobile.vin} value={automobile.vin}>
                          {automobile.vin}
                      </option>
                    );
                    }})}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={salesperson} onChange={handleSalesPersonChange} required  name="salesperson" className="form-select">
                    <option value="">Choose a Sales Person</option>
                    {salespersons.map(person => {
                            return (
                                <option key={person.id} value={person.id}>
                                    {person.first_name} {person.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={customer} onChange={handleCustomerChange} required name="customer" className="form-select">
                    <option value="">Choose a Customer</option>
                    {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={price} onChange={handleSalesPriceChange} placeholder="Sales Price" required type="text" name="price" id="price" className="form-control"/>
                    <label htmlFor="price">Sales Price</label>
                </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
    </div>
  </div>
  );
}

export default SalesForm;
