import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark border-bottom border-body">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarLink Central</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Manufacturers
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturer List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/manufacturers/create">Add New Manufacturer</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Vehicle Models
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/models">Models</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/models/create">Add Mew Model</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Automobiles
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/automobiles">Automobiles</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/automobiles/create">Add New Automobile</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Salespeople
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/salespeople">Salespeople</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/salespeople/create">Add New Salesperson</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Customer
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/customers">Customers</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/customers/create">Add New Customer</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/sales">Sales</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/sales/create">Record a New Sale</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/sales/history">Salesperson History</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Technicians
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/technician">Technicians</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/technician/create">Create a Technician</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Appointments
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/appointments">Service Appointments</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments/create">Create an Appointment</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments/history">Appointments History</NavLink></li>
                </ul>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
