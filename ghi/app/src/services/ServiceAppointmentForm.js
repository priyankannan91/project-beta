import React, { useEffect, useState } from "react";

function ServiceAppointmentForm() {
  const [technicians, setTechnicians] = useState([]);
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [technician, setTechnician] = useState('');
  const [reason, setReason] = useState('');

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleChangeCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleChangeDate = (event) => {
    const value = event.target.value;
    setDate(value);
  }

  const handleChangeTime = (event) => {
    const value = event.target.value;
    setTime(value);
  }

  const handleChangeTechnician = (event) => {
    const value = event.target.value;
    setTechnician(value);
  }

  const handleChangeReason = (event) => {
    const value = event.target.value;
    setReason(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.service_date = new Date(date).toISOString().slice(0, 10);
    data.service_time = `${time}:00`;
    data.technician = technician;
    data.reason = reason;

    const postAppointmentsUrl = "http://localhost:8080/api/appointments/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };

    const appointmentResponse = await fetch(postAppointmentsUrl, fetchOptions);

    if (appointmentResponse.ok) {
      console.log("success");
      setVin('');
      setCustomer('');
      setDate('');
      setTime('');
      setTechnician('');
      setReason('');
    }
  }

  const fetchData = async () => {
    const fetchAppointmentsUrl = "http://localhost:8080/api/technicians/";
    const fetchAppointmentsResponse = await fetch(fetchAppointmentsUrl);

    if (fetchAppointmentsResponse.ok) {
      const data = await fetchAppointmentsResponse.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeVin} value={vin} placeholder="VIN" required name="vin" type="text" id="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeCustomer} value={customer} placeholder="Customer" required name="customer" type="text" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeDate} value={date} placeholder="Service Date" required name="service_date" type="date" id="service_date" className="form-control" />
              <label htmlFor="service_date">Service Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeTime} value={time} placeholder="Service Time" required name="service_time" type="time" id="service_time" className="form-control" />
              <label htmlFor="service_time">Service Time</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeTechnician} value={technician} required name="technician" className="form-select" id="technician">
                <option value="">Technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.first_name} {technician.last_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeReason} value={reason} placeholder="Service Reason" required name="reason" type="text" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
          <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ServiceAppointmentForm;
