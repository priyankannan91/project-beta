import React, { useEffect, useState } from "react";

function ServiceAppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const appointmentsUrl = "http://localhost:8080/api/appointments/";
    const appointmentsResponse = await fetch(appointmentsUrl);

    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();

      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const cancel = async (event, id) => {
    const url =`http://localhost:8080/api/appointments/${id}/cancel/`
    const fetchConfig = {
      method: 'PUT',
      body: JSON.stringify({ status: 3 }),
      headers: {'Content-Type': 'application/json',}

    }

    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      fetchData()
    }
  };

  const finished = async (id) => {
    const url =`http://localhost:8080/api/appointments/${id}/finish/`
    const fetchConfig = {
      method: 'PUT',
      body: JSON.stringify({ status: 2 }),
      headers: {'Content-Type': 'application/json',}

    };

    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      fetchData()
    }
  };

  return (
    <div className="container">
      <div>
        <h1>Service Appointments</h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return(
              <tr key={ appointment.id }>
                <td>{ appointment.vin }</td>
                <td>{ appointment.vip === true ? "Yes" : "No" }</td>
                <td>{ appointment.customer }</td>
                <td>{ appointment.service_date }</td>
                <td>{ appointment.service_time }</td>
                <td>{ appointment.technician.first_name } { appointment.technician.last_name}</td>
                <td>{ appointment.reason }</td>
                <td>
                  <button onClick={event => cancel(event, appointment.id)} id={ appointment.id } type="button" className="btn btn-danger rounded-0">
                    Cancel
                  </button>
                  <button onClick={()=> finished(appointment.id)} id={ appointment.id } type="button" className="btn btn-success rounded-0">
                    Finished
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ServiceAppointmentList;
