import React, { useEffect, useState } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const [searchInput, setSearchInput] = useState('');

  const handleChangeSearchInput = (event) => {
    const value = event.target.value;
    setSearchInput(value);
  };

  const handleSearchButtonClick = () => {
    filterAppointments();
  };

  const filterAppointments = () => {
    const filtered = appointments.filter((appointment) =>
      appointment.vin.toLowerCase().includes(searchInput.toLowerCase())
    );
    setFilteredAppointments(filtered);
  };


  const fetchData = async () => {
    const appointmentsUrl = 'http://localhost:8080/api/appointments/';

    const appointmentsResponse = await fetch(appointmentsUrl);

    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();
      setAppointments(data.appointments);
      setFilteredAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div style={{ padding: 20 }}>
        <input type="search" placeholder="Search for VIN" onChange={handleChangeSearchInput} value={searchInput} />
        <button onClick={handleSearchButtonClick}>Search</button>
      </div>
      <div>
        <h1>Service Appointments</h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip === true ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.service_date}</td>
                <td>{appointment.service_time}</td>
                <td>
                  {appointment.technician.first_name}{' '}{appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ServiceHistory;
