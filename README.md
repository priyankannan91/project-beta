# CarLink Central

Team:

* Service Microservice - Priyanka Kannan
* Sale Microservice - Mina Nguyen

## About this project

This application's purpose is to digitize the data of a car dealership. From organizing inventory to scheduling appointments for maintenance, it handles all functionality regarding a car dealership through three microservices: Inventory, Service, and Sales. 

## Service microservice

The Service microservice includes an API for managing service appointments with endpoints for technicians and appointments. It also features an automobile poller to update vehicle data periodically. The React-based Front-End provides a user-friendly interface for creating, listing, and managing technicians and appointments, with a special VIP feature for sold vehicles and appointment status control, along with a service history search capability by VIN.

## Sales microservice

The sales microservice utilizes Restful APIs to interact with Automobile resources and features a poller to obtain vehicle data from the inventory Automobile microservice. The sales microservices utilizes four models: AutomobileVO, Salesperson, Customer, and Sales to complete the back-end portion of this application. React was used to create a dynamic front-end application, featuring a list and form for customers, salesperson, and sales, with a special feature to filter and view a salesperson's history of past sales.

## Built with
* Postgres
* ReactJS
* Docker
* Django
* Bootstrap

## Getting Started
Just a few things before you dive into the project!
* Docker Desktop

Installation
* Clone the repository into a directory of your choosing
* Use "docker volume build beta-data" command to build the volume
* Use the "docker-compose build" command to build the necessary images
* Then use the "docker-compose up" command to create the containers
* Head over to http://localhost:3000/ to interact with the application!
* In order to use the car application, don't forget to create an inventory of cars under the Inventory tab! 
