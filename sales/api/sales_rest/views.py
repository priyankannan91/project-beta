from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson list does not exist"},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create salesperson"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist! Try again!"})
            response.status_code=404
            return response
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.get(id=pk)
            props = [
                "first_name",
                "last_name"
            ]

            for prop in props:
                if prop in content:
                    setattr(salespeople, prop, content[prop])
            salespeople.save()
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist!"})
            response.status_code=404
            return response


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer list does not exist"},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create customer"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=pk)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist! Try again!"})
            response.status_code=404
            return response
    else: 
        try:
            content = json.loads(request.body)
            customers = Customer.objects.get(id=pk)
            properties = [
                "first_name",
                "last_name",
                "address",
                "phone_number",
            ]

            for prop in properties:
                if prop in content:
                    setattr(customers, prop, content[prop])
            customers.save()
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist!"})
            response.status_code=404
            return response


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale list does not exist"},
                status=400,
            )
    else: 
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (Sale.DoesNotExist,
                AutomobileVO.DoesNotExist,
                Salesperson.DoesNotExist,
                Customer.DoesNotExist):
            return JsonResponse(
                {"message": "Could not create sale"},
                status=404,
            )


@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, pk):
    try:
        sales = Sale.objects.get(id=pk)
    except Sale.DoesNotExist:
        response = JsonResponse({"message": "Sale does not exist"})
        response.status_code=404
        return response
    if request.method == "GET":
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
