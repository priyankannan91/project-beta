from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "service_date",
        "service_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
