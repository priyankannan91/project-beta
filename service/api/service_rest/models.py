from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.vin}'


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f'{self.first_name}'


class Appointment(models.Model):
    service_date = models.DateField()
    service_time = models.TimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="created")
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(blank=True, default=False, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
